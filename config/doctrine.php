<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

return [
    'orm' => [
        'query' => [
            'filters' => [
            ],
        ],
        'mapping' => [
            'paths' => [
                \Smtm\AuthConsumer\Context\Token\Domain\Token::class =>
                    __DIR__ . '/../src/Context/Token/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
