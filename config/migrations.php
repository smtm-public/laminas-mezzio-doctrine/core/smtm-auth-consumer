<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_auth_consumer',
    ],

    'migrations_paths' => [
        'Smtm\AuthConsumer\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
