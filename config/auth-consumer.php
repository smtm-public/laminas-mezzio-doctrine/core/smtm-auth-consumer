<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth-consumer')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-auth-consumer');
    $dotenv->load();
}

return [
    'entityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_ENTITY_MANAGER_NAME'),
    'readerEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_READER_ENTITY_MANAGER_NAME'),
    'archivedAccessEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_ARCHIVED_ACCESS_ENTITY_MANAGER_NAME'),
    'archivedAccessReaderEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_ARCHIVED_ACCESS_READER_ENTITY_MANAGER_NAME'),
    'rolePermissionEntityManagerName' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(['SMTM_AUTH_CONSUMER_DB_ROLE_PERMISSION_ENTITY_MANAGER_NAME']),
    'rolePermissionReaderEntityManagerName' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal(['SMTM_AUTH_CONSUMER_DB_READER_ROLE_PERMISSION_ENTITY_MANAGER_NAME']),
    'rolePermissionArchivedAccessEntityManagerName' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_ROLE_PERMISSION_ARCHIVED_ACCESS_ENTITY_MANAGER_NAME'),
    'rolePermissionArchivedAccessReaderEntityManagerName' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_DB_ROLE_PERMISSION_ARCHIVED_ACCESS_READER_ENTITY_MANAGER_NAME'),
    'automaticallyCreateAuthenticatedUsers' =>
        filter_var(
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_AUTOMATICALLY_CREATE_AUTHENTICATED_USERS'),
            FILTER_VALIDATE_BOOL
        ),
];
