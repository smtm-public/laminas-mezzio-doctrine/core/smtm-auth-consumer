<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Factory\AuthConfigAwareDelegator;
use Smtm\AuthConsumer\Authentication\Application\Service\Bearer\BearerAuthenticationService;
use Smtm\AuthConsumer\Context\Token\Application\Service\TokenService;
use Smtm\AuthConsumer\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\AuthConsumer\Factory\AuthConsumerConfigAwareDelegator;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                $applicationServicePluginManager->addDelegator(
                    TokenService::class,
                    AuthConfigAwareDelegator::class
                );
                $applicationServicePluginManager->setAlias(
                    TokenServiceInterface::class,
                    TokenService::class
                );

                return $applicationServicePluginManager;
            }
        ],

        AuthenticationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var AuthenticationServicePluginManager $authenticationServicePluginManager */
                $authenticationServicePluginManager = $callback();

                $authenticationServicePluginManager->addDelegator(
                    BearerAuthenticationService::class,
                    AuthConsumerConfigAwareDelegator::class
                );

                return $authenticationServicePluginManager;
            }
        ],

//        InfrastructureServicePluginManager::class => [
//            function (
//                ContainerInterface $container,
//                $name,
//                callable $callback,
//                array $options = null
//            ) {
//                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
//                $infrastructureServicePluginManager = $callback();
//                $infrastructureServicePluginManager->addDelegator(
//                    EntityManagerInterface::class,
//                    EnablePackageDoctineFiltersOnEntityMangerDelegator::class
//                );
//
//                return $infrastructureServicePluginManager;
//            }
//        ],
    ],
];
