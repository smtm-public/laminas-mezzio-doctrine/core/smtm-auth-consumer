<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

use Smtm\Auth\Authentication\Application\Service\Basic\BasicUserAuthenticationService;
use Smtm\Auth\Authentication\Application\Service\Bearer\BearerJwtAuthenticationService;
use Smtm\Auth\Authorization\Application\Service\AuthorizationService;
use Smtm\Base\Http\InputFilter\UuidRouteParamRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth-consumer')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-auth-consumer'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CONSUMER_EXPOSED_ROUTES'));
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    'smtm.auth-consumer.token.create' => [
        'path' => '/auth-consumer/token',
        'method' => 'post',
        'middleware' => Context\Token\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Jwt\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.token.refresh' => [
        'path' => '/auth-consumer/token/refresh',
        'method' => 'post',
        'middleware' => Context\Token\Http\Handler\Refresh\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
        ],
    ],

    'smtm.auth-consumer.title.create' => [
        'path' => '/auth-consumer/title',
        'method' => 'post',
        'middleware' => Context\Title\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => null,
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\Title\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.title.index' => [
        'path' => '/auth-consumer/title',
        'method' => 'get',
        'middleware' => Context\Title\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\Title\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],
    'smtm.auth-consumer.title.read' => [
        'path' => '/auth-consumer/title/{uuid}',
        'method' => 'get',
        'middleware' => Context\Title\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\Title\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.title.update' => [
        'path' => '/auth-consumer/title/{uuid}',
        'method' => 'put',
        'middleware' => Context\Title\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\Title\Http\InputFilter\UpdateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.title.delete' => [
        'path' => '/auth-consumer/title/{uuid}',
        'method' => 'delete',
        'middleware' => Context\Title\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],

    'smtm.auth-consumer.user.create' => [
        'path' => '/auth-consumer/user',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.user.index' => [
        'path' => '/auth-consumer/user',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],
    'smtm.auth-consumer.user.read' => [
        'path' => '/auth-consumer/user/{uuid}',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.user.update' => [
        'path' => '/auth-consumer/user/{uuid}',
        'method' => 'put',
        'middleware' => Context\User\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\UpdateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.user.delete' => [
        'path' => '/auth-consumer/user/{uuid}',
        'method' => 'delete',
        'middleware' => Context\User\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth-consumer.authenticated-user.read' => [
        'path' => '/auth-consumer/authenticated-user',
        'method' => 'get',
        'middleware' => Context\AuthenticatedUser\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.auth-consumer.client.index' => [
        'path' => '/auth-consumer/client',
        'method' => 'get',
        'middleware' => Context\Client\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerJwtAuthenticationService::class,
                    ]
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ],
            ],
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
