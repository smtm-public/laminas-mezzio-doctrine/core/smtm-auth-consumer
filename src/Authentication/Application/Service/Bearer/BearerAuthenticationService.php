<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Authentication\Application\Service\Bearer;

use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidJwtSignatureException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\TokenNotFoundException;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\AuthConsumer\Context\Token\Application\Service\TokenService;
use Smtm\AuthConsumer\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\AuthConsumer\Context\Token\Domain\Token;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Service\JwtService as JwtInfrastructureService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BearerAuthenticationService extends AbstractApplicationService implements
    BearerAuthenticationServiceInterface,
    ConfigAwareInterface
{

    use ConfigAwareTrait;

    protected ?Token $authenticatedToken = null;
    protected ?\Lcobucci\JWT\Token $parsedJwt = null;

    public function authenticateWithToken(
        string $token
    ): void {
        /** @var JwtInfrastructureService $jwtInfrastructureServiceAuthIssuer */
        $jwtInfrastructureServiceAuthIssuer = $this->infrastructureServicePluginManager->get(
            $this->config['jwtVerifierServiceName']
        );
        $parsedJwt = $jwtInfrastructureServiceAuthIssuer->parse($token);

        if (!$jwtInfrastructureServiceAuthIssuer->validate($parsedJwt)) {
            throw new InvalidJwtSignatureException();
        }

        $uuid = $jwtInfrastructureServiceAuthIssuer->getClaimJti($parsedJwt);
        /** @var TokenService $tokenService */
        $jwtService = $this->applicationServicePluginManager->get(TokenServiceInterface::class);

        try {
            /** @var Token $token */
            $token = $jwtService->getOneByUuid($uuid);
        } catch (EntityNotFoundException $e) {
            throw new TokenNotFoundException('', 0, $e);
        }

        $this->setAuthenticatedToken($token);
        $this->setParsedJwt($parsedJwt);
    }

    public function getAuthenticatedUserClient(): UserClientInterface
    {
        return $this->authenticatedToken->getUserClient();
    }

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedToken->getUserClient()->getUser();
    }

    public function getRoleCodeCollection(): array
    {
        return $this->authenticatedToken->getUserClient()->getRoleCodeCollection();
    }

    public function getAuthenticatedToken(): ?Token
    {
        return $this->authenticatedToken;
    }

    public function setAuthenticatedToken(?Token $authenticatedToken): static
    {
        $this->authenticatedToken = $authenticatedToken;

        return $this;
    }

    public function getParsedJwt(): ?\Lcobucci\JWT\Token
    {
        return $this->parsedJwt;
    }

    public function setParsedJwt(?\Lcobucci\JWT\Token $parsedJwt): static
    {
        $this->parsedJwt = $parsedJwt;

        return $this;
    }
}
