<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'auth-consumer' => 'array',
        'dependencies' => 'array',
        'doctrine' => 'array',
        'routes' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'auth-consumer' => include __DIR__ . '/../config/auth-consumer.php',
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'routes' => include __DIR__ . '/../config/routes.php',
        ];
    }
}
