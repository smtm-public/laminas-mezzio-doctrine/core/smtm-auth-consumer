<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthConsumerConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'auth-consumer';
}
