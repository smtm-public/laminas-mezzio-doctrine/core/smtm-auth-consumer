<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'accessToken' => 'You must specify an accessToken for the Token.',
        'expires' => 'You must specify an \'expires\' date for the Token.',
    ];
}
