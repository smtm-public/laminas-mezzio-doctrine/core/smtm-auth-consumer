<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Application\Service;

use DateTimeImmutable;
use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\AuthConsumer\Context\Token\Application\Hydrator\TokenHydrator;
use Smtm\AuthConsumer\Context\Token\Domain\Token;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenService extends AbstractDbService implements
    ConfigAwareInterface,
    UuidAwareEntityDbServiceInterface,
    TokenServiceInterface
{

    use ConfigAwareTrait, UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Token::class;
    protected ?string $hydratorName = TokenHydrator::class;

    public function revokeToken(UserInterface $user): void
    {
        /** @var OAuthTokenReaderService $oAuthTokenService */
        $oAuthTokenService = $this->applicationServicePluginManager->get(OAuthTokenReaderService::class);
        /** @var OAuthToken $oAuthToken */
        $oAuthToken = $oAuthTokenService->getOneOrNullBy(
            [ 'user.id' => $user->getId() ],
            orderBy: [ 'id' => 'DESC' ]
        );


        if ($oAuthToken) {
            $authenticatedTokenTokens = $this->getAll([
                'oAuthToken.id' => $oAuthToken->getId()
            ]);

            foreach ($authenticatedTokenTokens as $authenticatedTokenToken) {
                $this->update($authenticatedTokenToken, [
                    'revoked' => true,
                    'expires' => new DateTimeImmutable(),
                ]);
            }
        }
    }
}
