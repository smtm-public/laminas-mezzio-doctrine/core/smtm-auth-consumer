<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbReaderServiceInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TokenReaderServiceInterface extends ApplicationServiceInterface, DbReaderServiceInterface
{

}
