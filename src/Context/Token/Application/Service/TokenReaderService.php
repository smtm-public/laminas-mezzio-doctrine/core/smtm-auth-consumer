<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenReaderService extends TokenService implements TokenReaderServiceInterface
{
    protected ?string $entityManagerName = 'default-reader';
}
