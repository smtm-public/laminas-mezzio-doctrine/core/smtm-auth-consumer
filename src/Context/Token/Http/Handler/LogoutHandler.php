<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Http\Handler;

use Smtm\Auth\Context\Authentication\Context\User\Context\OAuthToken\Context\Token\Application\Extractor\TokenExtractor;
use Smtm\Auth\Context\Authentication\Http\Middleware\AuthenticationMiddleware;
use Smtm\Auth\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractCreateHandler;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * @author Angel Baev <angel@smtm.bg>
 */
class LogOutHandler extends AbstractCreateHandler
{
    public ?string $applicationServiceName = TokenServiceInterface::class;
    public ?string $domainObjectExtractorName = TokenExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        $authenticatedUser = $request->getAttribute(AuthenticationMiddleware::REQUEST_ATTRIBUTE_NAME_AUTHENTICATED_USER);

        if ($authenticatedUser) {
            $this->applicationService->revokeToken($authenticatedUser);
        }


        return $this->prepareResponse(
            [],
            static::parseQueryParams($request->getQueryParams()),
            HttpHelper::STATUS_CODE_CREATED
        );
    }
}
