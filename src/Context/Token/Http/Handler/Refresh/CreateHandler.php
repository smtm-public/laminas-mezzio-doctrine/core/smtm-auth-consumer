<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Http\Handler\Refresh;

use Smtm\Auth\Context\Authentication\Context\User\Context\OAuthToken\Context\Token\Domain\Token;
use Smtm\Auth\Context\Authentication\Context\User\Context\OAuthToken\Context\Token\Application\Service\Exception\AuthenticationProviderResponseException;
use Smtm\Auth\Context\Token\Application\Extractor\TokenExtractor;
use Smtm\Auth\Context\Token\Application\Service\TokenService;
use Smtm\Auth\Context\Token\Application\Service\TokenServiceInterface;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractCreateHandler;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CreateHandler extends AbstractCreateHandler
{
    public ?string $applicationServiceName = TokenServiceInterface::class;
    public ?string $domainObjectExtractorName = TokenExtractor::class;
    /** @var TokenService $applicationService */
    protected ?AbstractApplicationService $applicationService;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        try {
            /** @var Token $entity */
            $entity = $this->applicationService->refresh(
                $request->getParsedBody(),
                null,
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            );

            return $this->prepareResponse(
                $entity,
                static::parseQueryParams($request->getQueryParams()),
                HttpHelper::STATUS_CODE_CREATED
            );
        } catch (AuthenticationProviderResponseException $e) {
            throw new UnauthorizedException('', 0, $e);
        }
    }
}
