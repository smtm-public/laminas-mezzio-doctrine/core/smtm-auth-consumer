<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Context\Token\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\AuthConsumer\Context\Token\Domain\Token;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenFilter extends SQLFilter
{
    public const NAME = 'excludeSmtmAuthConsumerToken';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if ($targetEntity->getReflectionClass()->name === Token::class) {
            return sprintf(
                '%s.revoked = 0',
                $targetTableAlias
            );
        }

        return '';
    }
}
