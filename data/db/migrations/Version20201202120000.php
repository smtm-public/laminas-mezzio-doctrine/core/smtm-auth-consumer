<?php

declare(strict_types=1);

namespace Smtm\AuthConsumer\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createAuthConsumerTokenTable($schema);
    }

    public function createAuthConsumerTokenTable(Schema $schema): void
    {
        $authConsumerTokenTable = $this->createTableAndPrimaryKey($schema, 'auth_consumer_token');
        $this->addUuidStringColumnAndUniqueIndex($authConsumerTokenTable);
        $authConsumerTokenTable->addColumn('provider_uuid', Types::STRING, ['length' => 36, 'notNull' => true]);
        $authConsumerTokenTable->addColumn('auth_user_client_id', Types::INTEGER, ['notNull' => true]);
        $authConsumerTokenTable->addForeignKeyConstraint(
            $schema->getTable('auth_user_client'),
            ['auth_user_client_id'],
            ['id'],
            [],
            'fk_' . $authConsumerTokenTable->getName() . '_auth_user_client_id'
        );
        $authConsumerTokenTable->addIndex(['auth_user_client_id'], 'idx_' . $authConsumerTokenTable->getName() . '_auth_user_client_id');
        $authConsumerTokenTable->addColumn('id_token', Types::TEXT, ['notNull' => false]);
        $authConsumerTokenTable->addColumn('access_token', Types::TEXT, ['notNull' => true]);
        $authConsumerTokenTable->addColumn('refresh_token', Types::TEXT, ['notNull' => false]);
        $authConsumerTokenTable->addColumn('expires', Types::DATETIME_IMMUTABLE, ['notNull' => true]);
        $authConsumerTokenTable->addColumn('refresh_token_expires', Types::DATETIME_IMMUTABLE, ['notNull' => false]);
        $authConsumerTokenTable->addColumn('used', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authConsumerTokenTable->addIndex(['used'], 'idx_' . $authConsumerTokenTable->getName() . '_used');
        $authConsumerTokenTable->addColumn('revoked', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authConsumerTokenTable->addIndex(['revoked'], 'idx_' . $authConsumerTokenTable->getName() . '_revoked');
        $authConsumerTokenTable->addColumn('token_type', Types::STRING, ['length' => 32, 'notNull' => false]);
        $authConsumerTokenTable->addColumn('r_scope', Types::STRING, ['length' => 255, 'notNull' => false]);
        $authConsumerTokenTable->addIndex(
            ['auth_user_client_id', 'used', 'revoked'],
            'idx_' . $authConsumerTokenTable->getName() . '_auth_user_client_id_used_revoked'
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authConsumerTokenTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authConsumerTokenTable);
        $this->addNotArchivedSmallintColumnAndIndex($authConsumerTokenTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authConsumerTokenTable);
    }

    public function down(Schema $schema): void
    {

    }
}
